package com.example.dialogdemo;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.widget.TextView;

public class HelloDialog extends DialogFragment {

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Activity activity = getActivity();
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setTitle(R.string.dialg_world_title);
        TextView content = new TextView(activity);
        content.setText(R.string.dialg_world_content);
        builder.setView(content);
        return builder.create();
    }

    public static DialogFragment newInstance() {
        return new HelloDialog();
    }
}
