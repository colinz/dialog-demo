package com.example.dialogdemo;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.FragmentManager;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;

public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        findViewById(R.id.show_dialog).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                showDialog(Constants.Dialog.HELLO);
            }
        });

        findViewById(R.id.show_dialog_fragment).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                showHelloDialog();
            }
        });

    }

    @Override
    protected Dialog onCreateDialog(int id) {
        Dialog dialog = null;
        switch (id) {
            case Constants.Dialog.HELLO:
                dialog = createHelloDialog();
                break;
            case Constants.Dialog.WORLD:
                dialog = createWorldDialog();
        }
        return dialog;
    }

    private Dialog createHelloDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.dialg_hello_title);
        TextView content = new TextView(this);
        content.setText(R.string.dialg_hello_content);
        builder.setView(content);
        return builder.create();
    }

    private Dialog createWorldDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.dialg_world_title);
        TextView content = new TextView(this);
        content.setText(R.string.dialg_world_content);
        builder.setView(content);
        return builder.create();
    }

    private void showHelloDialog() {
        DialogFragment dialog = HelloDialog.newInstance();
        FragmentManager manager = getFragmentManager();
        String tag = Constants.Dialog.DIALOG_HELLO;
        dialog.show(manager, tag);
    }

}
