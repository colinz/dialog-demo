package com.example.dialogdemo;

public class Constants {

    public static class Dialog {
        public static final int HELLO = 1;
        public static final int WORLD = 2;

        public static final String DIALOG_HELLO = "dialog_hello";
        public static final String DIALOG_WORLD = "dialog_world";
    }
}
